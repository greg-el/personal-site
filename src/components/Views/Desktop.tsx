import React from "react";
import { newTracker, trackPageView } from "@snowplow/browser-tracker";

interface IState {}

interface IProps {}

class Desktop extends React.Component<IProps, IState> {
  componentDidMount() {
    newTracker("sp1", "0.0.0.0:9090", {
      appId: "portfolio",
      plugins: [],
    });
    trackPageView();
  }
  render() {
    return (
      <div id="desktop-wrapper">
        <div id="desktop">{this.props.children}</div>
      </div>
    );
  }
}

export default Desktop;
