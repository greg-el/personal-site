![](Snowplow.gif)
_Performing and capturing events in the app whilst the &nbsp;`micro/all`&nbsp;endpoint displays them_

<br>

# About

This is my portfolio site, now with Snowplow micro integrated for tracking site interaction.

This project is comprised of two containers

- snowplow_micro - The Snowplow Micro docker image
- portfolio - The web app with micro intergrated

# Running This Project

## Requirements

- Install Docker for your OS from:&nbsp; https://docs.docker.com/get-docker/

- Install docker-compose:&nbsp;
  https://docs.docker.com/compose/install/

## Running

Navigate to the root of this project and run the following command:

    docker-compose up --build

## Snowplow Micro

The following Snowplow Micro endpoints will be available with the containers running:

- http://localhost:9090/micro/all

- http://localhost:9090/micro/good

- http://localhost:9090/micro/bad

- http://localhost:9090/micro/reset

## App

The app is now available at the following address with the containers running:

http://localhost:3000/
